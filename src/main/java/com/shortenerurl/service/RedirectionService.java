package com.shortenerurl.service;

public interface RedirectionService {
    String doRedirect(String link);
}
