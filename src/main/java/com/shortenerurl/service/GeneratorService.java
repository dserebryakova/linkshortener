package com.shortenerurl.service;

public interface GeneratorService {
    String generateShortUrl(String original);
}
