package com.shortenerurl.service;

import com.shortenerurl.service.impl.UrlStatistic;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StatisticService {
    List<UrlStatistic> getAllRankedUrl(Pageable pageable);

    UrlStatistic getRankedUrlByShortLink(String link);
}
