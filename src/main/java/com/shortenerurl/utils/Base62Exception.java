package com.shortenerurl.utils;

public class Base62Exception extends RuntimeException {
    Base62Exception(String message) {
        super(message);
    }
}
